$pdflatex = "lualatex --shell-escape %O %S";
$pdf_mode = 1; $postscript_mode = $dvi_mode = 0;

$clean_ext = "";
$clean_full_ext = "%R_minted-* nav snm vrb";


$always_view_file_via_temporary = 1;
$pdf_previewer = "evince %O %S";
