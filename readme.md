[![pipeline status](https://gitlab.com/crafting-software/articles/badges/master/pipeline.svg)](https://gitlab.com/crafting-software/articles/commits/master)

Ce projet contient des articles et d'autres contenus autour de software craftsmanship.

[Book](https://gitlab.com/crafting-software/articles/builds/artifacts/master/file/crafting-software.pdf?job=latex)
